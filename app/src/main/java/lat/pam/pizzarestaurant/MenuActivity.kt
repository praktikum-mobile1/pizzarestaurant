package lat.pam.pizzarestaurant

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.parcelize.Parcelize

@Parcelize
class MenuActivity : AppCompatActivity(), Parcelable {
    private lateinit var rvMenu: RecyclerView
    private val list = ArrayList<Menu>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        rvMenu = findViewById(R.id.rv_menu)
        rvMenu.setHasFixedSize(true)
        list.addAll(getListMenu())
        showRecyclerList()
    }

    private fun getListMenu(): ArrayList<Menu> {
        val dataName = resources.getStringArray(R.array.data_menu_name)
        val dataDescription = resources.getStringArray(R.array.data_menu_descripstion)
        val dataPhoto = resources.obtainTypedArray(R.array.data_menu_photo)
        val listMenu = ArrayList<Menu>()
        for (i in dataName.indices){
            val menu = Menu(dataName[i], dataDescription[i], dataPhoto.getResourceId(i, -1))
            listMenu.add(menu)
        }
        return listMenu
    }

    private fun showRecyclerList(){
        rvMenu.layoutManager = LinearLayoutManager(this)
        val listMenuAdapter = ListMenuAdapter(list)
        rvMenu.adapter = listMenuAdapter

        listMenuAdapter.setOnItemClickCallback(object : ListMenuAdapter.OnItemClickCallback{
            override fun onItemClicked(data: Menu){
                showSelectedMenu(data)
            }
        })
    }

    private fun showSelectedMenu(menu: Menu){
        Toast.makeText(this, "Anda memilih " + menu.name, Toast.LENGTH_SHORT).show()
    }
}