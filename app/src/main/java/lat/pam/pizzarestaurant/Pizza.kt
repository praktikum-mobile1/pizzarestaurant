package lat.pam.pizzarestaurant

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class Pizza : AppCompatActivity() {
    private lateinit var imgPizza:ImageView
    private lateinit var tvName:TextView
    private lateinit var tvPrice:TextView
    private lateinit var tvDescription: TextView
    private lateinit var btnOrder:Button
    private lateinit var btnBack:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pizza)

        imgPizza = findViewById(R.id.img_detail_photo)
        tvName = findViewById(R.id.tv_detail_name)
        tvPrice = findViewById(R.id.tv_price)
        tvDescription = findViewById(R.id.tv_detail_description)
        btnBack = findViewById(R.id.btn_back)
        btnOrder = findViewById(R.id.btn_order)

        btnBack.setOnClickListener {
            val backIntent = Intent (this@Pizza, MenuActivity::class.java)
            startActivity(backIntent)
        }

        btnOrder.setOnClickListener {
            val orderIntent = Intent (this@Pizza, OrderActivity::class.java)
            startActivity(orderIntent)
        }

    }
}