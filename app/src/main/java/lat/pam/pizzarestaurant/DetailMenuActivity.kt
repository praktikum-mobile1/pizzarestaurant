package lat.pam.pizzarestaurant

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class DetailMenuActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_MENU = "extra_menu"
    }

    private lateinit var tvDetailName: TextView
    private lateinit var tvDescriptionMenu: TextView
    private lateinit var imgPhoto: ImageView
    private lateinit var btnBack:Button
    private lateinit var btnOrder:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_menu)

        val dataMenu = if (Build.VERSION.SDK_INT >= 33) {
            intent.getParcelableExtra<Menu>(EXTRA_MENU, Menu::class.java)
        } else {
            @Suppress("DEPRECATION")
            intent.getParcelableExtra<Menu>(EXTRA_MENU)

        }

        if (dataMenu != null) {
            tvDetailName = findViewById(R.id.tv_detail_name)
            tvDescriptionMenu = findViewById(R.id.tv_detail_description)
            imgPhoto = findViewById(R.id.img_detail_photo)

            tvDetailName.text = dataMenu.name
            tvDescriptionMenu.text = dataMenu.description
            imgPhoto.setImageResource(dataMenu.photo)
        }


        btnOrder = findViewById(R.id.btn_order)
        btnBack = findViewById(R.id.btn_back)

        btnBack.setOnClickListener {
            val backIntent = Intent (this@DetailMenuActivity, MenuActivity::class.java)
            startActivity(backIntent)
        }

        btnOrder.setOnClickListener {
            val orderIntent = Intent (this@DetailMenuActivity, OrderActivity::class.java)
            startActivity(orderIntent)
        }
    }
}